const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

function ask(question) {
    return new Promise((resolve, reject) => 
        rl.question(question, (answer) => {
            rl.close();
            resolve(answer);
        })
    );
}

module.exports = {
    ask,
    interface: rl,
};